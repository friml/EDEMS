# EDEMS
Educational DEmonstrative Microprocessor Simulator is educational application for microprocessors.

![img](img)

## Manual
Usage manual is placed in [EDEMS/manual/manual.md](https://gitlab.com/friml/EDEMS/blob/develop/manual/manual.md)

### TL;DR
This project was created to ease demonstration of functions of basic microprocessor at universities. This is **not** a simulator of existing microprocessor. This is simulator of educational microprocesor, which architecture (EDEM) was designed to be as simple as possible, while still having possibility to demonstrate all basic functions and function blocks of a real microprocessor.  

## Demo
Feel free to use demo on [http://friml.gitlab.io/EDEMS/](http://friml.gitlab.io/EDEMS/).

## Installation
### Without server
- Clone this project 
```
git clone https://gitlab.com/friml/EDEMS.git
```
- Double click `index.html`. That simple
### With server
- Clone this project in directory, from which your configured server serves your static pages. For example using NGINX
```
git clone https://gitlab.com/friml/EDEMS.git /var/www/yourpage.com
```
- Check `yourpage.com`. That simple.

## Contribution guide
1. Don't be afraid to open an issue
2. Fork this project 
3. make your changes 
4. open a pull request

## Development requirements
node.js (tested on 14.15.1 LTS) with npm

### Installing required packages
run `npm install --production` in this directory
run `npm install` in this directory

### Developing
run `npm run-script watch` to start watchify process, which ensures, that `edems.js` file is updated on every refresh of browser

## Changelog
### v1.1.0
- IO LEDs added
- Minor improvements

### v1.0.0
- First working version